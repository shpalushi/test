package sample;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class MainWindowController implements Initializable {
    @FXML
    private Button lista_manual;

    @FXML
    private Button lista_excel;

    @FXML
    private Button reset_klient;

    @FXML
    private RadioButton lista_kliente_radio;

    @FXML
    private RadioButton lista_furnitore_radio;

    @FXML
    private RadioButton fature_blerje_radio;

    @FXML
    private RadioButton fature_shitje_radio;

    @FXML
    private RadioButton lista_borxhlinj_radio;

    @FXML
    private RadioButton nipt_radio;

    @FXML
    private RadioButton emer_radio;

    @FXML
    private ComboBox company_combo;


    @FXML
    private TableView<ImportFromDatabase> table_info;

    @FXML
    private TableColumn<ImportFromDatabase, String> column_nipt;

    @FXML
    private TableColumn<ImportFromDatabase, String> column_description;

    @FXML
    private TableColumn<ImportFromDatabase, String> column_date;

    private ObservableList<ImportFromDatabase> dataInformation;

    VoidMethods voidMethods = new VoidMethods();
    String filename = "C:\\Crawler\\serial\\login.txt";
    String ip = voidMethods.readFileLogin(filename).get(0);
    String database = voidMethods.readFileLogin(filename).get(1);
    String name = voidMethods.readFileLogin(filename).get(2);
    String password = voidMethods.readFileLogin(filename).get(3);
    Connection connection = voidMethods.setConnection(ip,database,name,password);


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ToggleGroup toggleGroup = new ToggleGroup();
        lista_kliente_radio.setToggleGroup(toggleGroup);
        lista_furnitore_radio.setToggleGroup(toggleGroup);
        fature_blerje_radio.setToggleGroup(toggleGroup);
        fature_shitje_radio.setToggleGroup(toggleGroup);
        lista_borxhlinj_radio.setToggleGroup(toggleGroup);
        emer_radio.setToggleGroup(toggleGroup);
        nipt_radio.setToggleGroup(toggleGroup);

        VoidMethods voidMethods = new VoidMethods();
        ObservableList<String> combobox_details = voidMethods.readCompaniesFromDatabase(connection);
            company_combo.setItems(combobox_details);
            company_combo.setValue("Choose the company...");
    }

    @FXML
    public void onCheckBoxChecked(){


        try{
            dataInformation = FXCollections.observableArrayList();

            ResultSet resultSet = connection.createStatement().executeQuery("SELECT * from passive_client");

            while (resultSet.next()){
                dataInformation.add(new ImportFromDatabase(resultSet.getString(1), resultSet.getString(2), resultSet.getString(4)));
            }
        }catch (SQLException e){
           e.printStackTrace();
        }

        column_nipt.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getNipt()));
        column_description.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getName()));
        column_date.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getDate()));

        table_info.setItems(null);
        table_info.setItems(dataInformation);
    }

}
