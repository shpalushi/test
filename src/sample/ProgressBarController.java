package sample;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ProgressBarController implements Initializable {
    @FXML
    private ProgressBar progress_bar;
    @FXML
    private ProgressIndicator progress_indicator;
    @FXML
    private Button close_button;
    @FXML
    private Label downloaded_label;
    @FXML
    private VBox close_button_vbox;
    @FXML
    private Label continue_label;
    @FXML
    private HBox hbox_progress;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        close_button_vbox.getChildren().removeAll(close_button, downloaded_label,continue_label);
        hbox_progress.getChildren().removeAll(downloaded_label);
        VoidMethods voidMethods = new VoidMethods();
        String linkAdress = voidMethods.getCsvDownloadLink();
        Task<Void> task = new DownloadClass(hbox_progress,progress_indicator,linkAdress, close_button_vbox, downloaded_label,close_button, continue_label);
        progress_bar.progressProperty().bind(task.progressProperty());
       // progress_indicator.progressProperty().bind(task.progressProperty());
        Thread thread1 = new Thread(task);
        thread1.setDaemon(true);
        thread1.start();


    }

    @FXML
    public void closeStage(){
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("main_window.fxml"));
        }catch (IOException e){
            e.printStackTrace();
        }
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        Stage oldStage = (Stage) close_button.getScene().getWindow();
        // do what you have to do
        oldStage.close();
        stage.show();
    }
}