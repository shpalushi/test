package sample;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ImportFromDatabase{
     private StringProperty nipt;
     private StringProperty name;
     private StringProperty drt;
     private StringProperty date;

    public ImportFromDatabase(String nipt, String name, String date) {
        this.nipt = new SimpleStringProperty(nipt);
        this.name = new SimpleStringProperty(name);
        //this.drt = new SimpleStringProperty(drt);
        this.date = new SimpleStringProperty(date);
    }

    public String getNipt() {
        return nipt.get();
    }

    public StringProperty niptProperty() {
        return nipt;
    }

    public void setNipt(String nipt) {
        this.nipt.set(nipt);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getDrt() {
        return drt.get();
    }

    public StringProperty drtProperty() {
        return drt;
    }

    public void setDrt(String drt) {
        this.drt.set(drt);
    }

    public String getDate() {
        return date.get();
    }

    public StringProperty dateProperty() {
        return date;
    }

    public void setDate(String date) {
        this.date.set(date);
    }
}
