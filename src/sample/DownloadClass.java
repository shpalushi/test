package sample;

import javafx.concurrent.Task;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;


public class DownloadClass extends Task<Void> {

    private VBox button_close_vbox;
    private Button button_close;
    private Label downloaded_label;
    private Label continue_label;
    private ProgressIndicator progress_round;
    private HBox progress_hbox;


    private String url;
    public DownloadClass(HBox progress_hbox,ProgressIndicator progress_round, String url, VBox button_close_vbox, Label downloaded_label, Button button_close, Label continue_label){

        this.progress_hbox = progress_hbox;
        this.progress_round = progress_round;
        this.url = url;
        this.button_close_vbox = button_close_vbox;
        this.downloaded_label = downloaded_label;
        this.button_close = button_close;
        this.continue_label = continue_label;

    }

    @Override
    protected Void call() throws Exception {
        VoidMethods voidMethods = new VoidMethods();
        File fileTest = new File("C:\\Crawler\\downloads"+"\\" + voidMethods.dateStringGenerator()+".csv");
        if (!fileTest.exists() ) {
            voidMethods.deleteDirectory("C:\\Crawler\\downloads");
            System.out.println("Deleting directory...");
        }
        String path = "C:\\Crawler\\downloads";
        voidMethods.makeDirectories(path);
        String downloadPlace = path + "\\"+voidMethods.dateStringGenerator()+".csv";
        URLConnection connection = new URL(url).openConnection();
        long fileLength = connection.getContentLengthLong();
        try(InputStream inputStream = connection.getInputStream();
            OutputStream outputStream = Files.newOutputStream(Paths.get(downloadPlace))){
            long nread = 0L;
            byte[] buf = new byte[8192];
            int n;
            while((n=inputStream.read(buf))>0){
                outputStream.write(buf,0,n);
                nread+=n;
                updateProgress(nread,fileLength);
            }

            importToDatabase("drop table if exists passive_client;\n" +
                    "create table passive_client(nipt text, name text, drt text, date text);" +
                    "CREATE table if not exists passive_client(nipt text, name text, drt text, date text );");


            return null;
//
        }

    }

    @Override
    protected void succeeded() {
        System.out.println("Succeeded");
        button_close_vbox.getChildren().addAll(continue_label,button_close);
        progress_hbox.getChildren().removeAll(progress_round);
        progress_hbox.getChildren().add(downloaded_label);
    }
    @Override
    protected void failed(){
        System.out.println("Failed");
    }



    public void deleteOldFiles(File directory){
        for(File file: directory.listFiles())
            if (!file.isDirectory())
                file.delete();
    }

    public void importToDatabase(String sql){
        VoidMethods voidMethods = new VoidMethods();
        String filename = "C:\\Crawler\\serial\\login.txt";
        String downloadedFile = "C:\\Crawler\\downloads\\" + voidMethods.dateStringGenerator() + ".csv";
        System.out.println(filename);
        //preparedQuery.getConnection(voidMethods.setConnection(ip,database,username,password));
        voidMethods.readFileLogin(filename);
        ArrayList<String> loginInfo = new ArrayList<>(voidMethods.readFileLogin(filename));
        List<List<String>> tableInformation = new ArrayList<>(voidMethods.getTheCsvInformation(downloadedFile));
        Connection dbConnection = voidMethods.setConnection(loginInfo.get(0), loginInfo.get(1), loginInfo.get(2), loginInfo.get(3));
        try

        {
            PreparedStatement prepare = dbConnection.prepareStatement(sql);
            prepare.execute();
            System.out.println("The query executed successfully.");
        }catch(
                SQLException e)

        {
            System.out.println("there was an error on your sql query.");
        }

        for(
                List<String> passiveClient :tableInformation){
            if (passiveClient.get(0).length()<4 ) {
                break;
            }else if(passiveClient.get(1).indexOf('\'')>=0){
                passiveClient.set(1,"not known");
            }
            try {
                String sqlQuery = "INSERT into passive_client(nipt, name, drt, date)" +
                        "values (\'" + passiveClient.get(0) + "\',\'" + passiveClient.get(1)
                        + "\',\'" + passiveClient.get(2) + "\',\'" + passiveClient.get(3) + "\')";
                System.out.println(sqlQuery);
                PreparedStatement insertToDatabase =
                        dbConnection.prepareStatement(sqlQuery);
                insertToDatabase.execute();
            } catch (SQLException e) {
                System.out.println("Sql exception while inserting to database.");
            }

        }
    }

}
