package sample;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import javax.swing.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class VoidMethods {
    public String dateStringGenerator(){
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        String myDate = dateFormat.format(cal.getTime());
        return myDate;
    }

    public void makeDirectories(String path){

        File makeDirectory =new File( path);
        new File(path).mkdirs();
    }

    public void deleteDirectory(String destination){
        try {
            FileUtils.deleteDirectory(new File(destination));
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    String getCsvDownloadLink() {

        String getTheCsvFileLink = "";
        try {
            Document doc = Jsoup.connect("https://www.tatime.gov.al/c/8/129/132/subjekte-pasive").userAgent("Mozilla/5.0").timeout(30000).get();
            Element temp =  doc.select("li.csv>a").first();
            getTheCsvFileLink = temp.absUrl("abs:href");

            System.out.println("srcPasiv attribute is : " + getTheCsvFileLink);
            System.out.println("get elements " + temp.tagName() + " " + temp.attr("abs:srcPasiv"));

        } catch (IOException ex) {
            System.out.println("There was an exception finding the link");

        }
        return getTheCsvFileLink;
    }

//    public void Test() throws IOException {
//            try (
//                    Reader reader = Files.newBufferedReader(Paths.get("C:\\Crawler\\downloads" + "\\" + dateStringGenerator()+".csv"));
//
//                    CSVReader csvReader = new CSVReader(reader);
//            ) {
//                // Reading Records One by One in a String array
//                String[] nextRecord;
//                while ((nextRecord = csvReader.readNext()) != null) {
//                    //System.out.println("Name : " + nextRecord[0]);
//                    String[] results = nextRecord[0].split("\\|");
//                    System.out.println(results[0] + results[1] + results[2] + results[3]);
////                System.out.println("Email : " + nextRecord[1]);
////                System.out.println("Phone : " + nextRecord[2]);
////                System.out.println("Country : " + nextRecord[3]);
//                    System.out.println("==========================");
//                }
//            }
//    }
    public List<List<String>> getTheCsvInformation(String filename){
        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split("\\|");
                records.add(Arrays.asList(values));
            }
        }catch (IOException e){
            System.out.println("no no no");
        }
        return records;
    }

    public Connection setConnection(String myIp, String myDatabase, String myUsername, String myPassword){
        try {
            String url = "jdbc:postgresql://"+myIp+"/"+ myDatabase;
            Connection connection = DriverManager.getConnection(url,myUsername,myPassword);
            System.out.println("Connected successfully");
            return connection;
        }catch (Exception e){
            System.out.println("Exception while connecting to database");
            e.getMessage();
        }
        return null;
    }

    public ArrayList<String> readFileLogin(String filename){
        AesEncryption aes = new AesEncryption();
        FileReader fileReader;
        ArrayList<String> datalist = null;
        try {

            fileReader = new FileReader(filename);
            BufferedReader br = new BufferedReader(fileReader);
            String line;
            datalist = new ArrayList<>();

            while ((line = br.readLine()) != null) {
                String dataInformation = aes.decrypt(line);
                datalist.add(dataInformation);
            }

        }catch (IOException e){
            e.printStackTrace();
            System.out.println("Filereader not found");
        }
        return datalist;
    }

    public ObservableList<String> readCompaniesFromDatabase(Connection connection){
        ObservableList<String> companies = null;
        try {
            ResultSet resultSet = connection.createStatement().executeQuery("SELECT company_name from company");
             companies = FXCollections.observableArrayList();
            while (resultSet.next()) {
                companies.add(resultSet.getString(1));
            }
        }catch (SQLException e){
            e.getMessage();
        }
        return companies;
    }

}
