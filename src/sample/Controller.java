package sample;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.apache.commons.io.IOUtils;

import javax.swing.*;
import java.io.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private TextField ip_text;

    @FXML
    private TextField port_text;

    @FXML
    private ComboBox<String> program_combo;

    @FXML
    private TextField username_text;

    @FXML
    private TextField password_text;

    @FXML
    private TextField db_name;

    @FXML
    private Button login_button;

    @FXML
    private Label connect_checker;

    String path = null;
    String fullPathname;

    @Override
    public void initialize(URL location, ResourceBundle resources) {


            File fileTest = new File("C:\\Crawler\\serial\\login.txt");
            if (fileTest.exists() && !fileTest.isDirectory()) {
                changeStage("main_window.fxml", true);
            }


        program_combo.getItems().addAll("Elif", "Bilanc", "FX", "Alpha", "Financa 5");
        path = "C:\\Crawler\\serial";
        VoidMethods createDir = new VoidMethods();
        createDir.makeDirectories(path);


    }

    @FXML
    public void loginButtonClicked(Event event){
        VoidMethods voidMethods = new VoidMethods();
        voidMethods.setConnection(ip_text.getText(), db_name.getText(), username_text.getText(), password_text.getText());
//        this.isConnectionCorrect = setConnection(ip_text.getText(), db_name.getText(), username_text.getText(), password_text.getText());

        if (voidMethods.setConnection(ip_text.getText(), db_name.getText(), username_text.getText(), password_text.getText())!= null) {//setConnection defined method below
            connect_checker.setTextFill(Color.GREEN);
            connect_checker.setText("CONNECTED");
            this.fullPathname = path + "\\login.txt";
            fileWriter(fullPathname);
            ((Node) event.getSource()).getScene().getWindow().hide();

            changeStage("progress_bar.fxml",false);                    //defined method below

        }

    }



    private void fileWriter(String filename){
        AesEncryption aes = new AesEncryption();
            File file = new File(filename);
            if (!file.exists()) {
                try (FileWriter fw = new FileWriter(file)) {



                    String ipEncrypted = aes.encrypt(ip_text.getText());
                    String portEncrypted = aes.encrypt(port_text.getText());
                    String dbNameEncrypted = aes.encrypt(db_name.getText());
                    String usernameEncrypted = aes.encrypt(username_text.getText());
                    String passwordEncrypted = aes.encrypt(password_text.getText());
                    System.out.println(passwordEncrypted);

                    fw.write(ipEncrypted);
                    fw.write(System.lineSeparator());
                    fw.write(dbNameEncrypted);
                    fw.write(System.lineSeparator());
                    fw.write(usernameEncrypted);
                    fw.write(System.lineSeparator());
                    fw.write(passwordEncrypted);
                    fw.write(System.lineSeparator());
                    fw.write(portEncrypted);
                    System.out.println("Wrote successfully");

                } catch (IOException e) {
                    System.out.println("Error opening the page");
                    e.getMessage();
                }

            }else{
                System.out.println("The file was created before. The information must be correct");
            }

    }

    public void changeStage (String newWindow, boolean resizable){
        try {
            Parent root = FXMLLoader.load(getClass().getResource(newWindow));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setResizable(resizable);
            stage.show();
        }catch (IOException e){
            System.out.println("Couldn't change the screen");
        }
    }


}
