package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = null;
        File fileTest = new File("C:\\Crawler\\serial\\login.txt");
        if(fileTest.exists() && !fileTest.isDirectory()) {
            VoidMethods voidMethods = new VoidMethods();
            String downloadPlace = "C:\\Crawler\\downloads"+"\\" + voidMethods.dateStringGenerator()+".csv";
            File fileTest2 = new File(downloadPlace);
            if (fileTest2.exists() && !fileTest2.isDirectory()) {
                root = FXMLLoader.load(getClass().getResource("main_window.fxml"));
            }else {
                root = FXMLLoader.load(getClass().getResource("progress_bar.fxml"));
                primaryStage.setResizable(false);

            }

        }else {
            root = FXMLLoader.load(getClass().getResource("login_controller.fxml"));
        }
        primaryStage.setTitle("SUBJEKTET PASIVE");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }


}
